const express = require("express")
const app = express()

const WebSocket = require("ws")
const wss = new WebSocket.Server({ port: 8082 });

app.set('view engine', 'ejs')
app.get('/', (req, res) => {
    console.log("here")
    res.render("gamepad")
})

app.listen(3000)


wss.on('connection', (socket, req) => {

    console.log(`connection from ${ req.socket.remoteAddress }`);
  
    // received message
    socket.on("message", (msg, isBinary) => {
      
      //var obj = JSON.parse(msg)

        console.log(msg.toString() )
      // broadcast to all clients
    //   ws.clients.forEach(client => {
    //     client.readyState === WebSocket.OPEN && client.send(msg, { binary });
    //   });
  
    });
  
    // closed
    socket.on('close', () => {
      console.log(`disconnection from ${ req.socket.remoteAddress }`);
    });
  
  });